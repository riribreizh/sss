#!/usr/bin/env sh

DESTDIR=/usr/local
cd `dirname $0`

if [ x"$1" = 'x-u' ]; then
	[ -z "$2" ] || DESTDIR="$2"
	rm -vf "$DESTDIR/bin/sss"
	rm -vrf "$DESTDIR/share/sss"
else
	[ -z "$1" ] || DESTDIR="$1"
	install -v -d "$DESTDIR/bin"
	install -v "bin/sss" "$DESTDIR/bin"

	install -v -d "$DESTDIR/share/sss"
	find "share/sss/" -type d -exec install -v -d "$DESTDIR/{}" \;
	for f in `find "share/sss" -type f`; do
		cp -v "$f" "$DESTDIR/`dirname $f`"
#		echo "$DESTDIR/`dirname $f`/`basename $f`"
	done
	#-exec install -v {} "$DESTDIR
fi


