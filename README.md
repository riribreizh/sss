# sss - Screeenshot/Screencast/Save

**sss** is highly inspired from [teiler](https://github.com/carnager/teiler), which was not exactly what I needed. **sss** tends to look more like a small app, but uses about the same tools:
* *bash*
* *rofi* for the menu
* *slop* for selection
* *maim* for screenshot capture
* *ffmpeg* for screencast capture (optional)
* *xclip* for clipboard interaction (optional)

## Features

* menu or command line driven
* takes screenshots (pictures), screencasts (videos) or cliboard (text)
* save to local hard drive (optional)
* upload to online platforms (optional, can be customized)
* copy resulting link (local file or URL if upload) in clipboard
* highly configurable to each one taste

## Installing

The project directory follows the standard Linux scheme, so a basic copy of needed files in `/usr/local` is provided with `install.sh`, but it can be used directly from the project tree. To install:
```sh
# destdir=/usr # /usr/local if not specified
# ./install.sh "$destdir"
```

Adding `-u` permits to uninstall:
```sh
# ./install -u "$destdir"
```

## Usage

The combinaison of user configuration and command line options permits to be very flexible to the behavior of **sss**, providing interactive or automated action. The most basic way to use it is to start without argument, then the main menu will appear:

![main menu screenshot](./main-menu.png "Main menu")

Let's dig into options (which is a sub-menu, denoted by the `▷` character).

![options menu screenshot](./options-menu.png "List of available options")

You can change any of these options for this run of **sss**, by selecting it (with `Enter` key). Those options are taken in order of precedence:
* on command line options
* in your configuration file in `$XDG_CONFIG_HOME/sss/config` if it exists
* in default options of the program

You can save the current config with the `⛃ Save config` command, so that it becomes the default without command line option.

Sub-menus permit to select the uploader plugin to use for each type of supported document: images (screenshots), videos (screencasts) or text (pastebin).

Command line options are available with `sss -h`:
```
sss - Screenshot/Screencast/Save - a teiler variant
(C) Richard Gill <richard@houbathecat.fr>
Usage: sss [action] [option, option...]
Main action options
-h, --help                    This help
-s, --shot, --screenshot      Make a region or window screenshot
-f, --full, --fullscreen      Make a fullscreen screenshot
-p, --paste, --pastetext      Save clipboard text
-l, --list, --list-uploaders  List known uploaders
-d, --dump, --dump-config     Dump current configuration (including commandline options)

Options:
-i, --interactive   Force interactive menus (if not interactive in config)
--no-decorations    Don't include window decorations in window screenshots
--decorations       Include window decorations in window screenshots
--hide-cursor       Don't capture mouse cursor in screenshots
--show-cursor       Capture mouse cursor in screenshots
--no-clipboard      Don't copy result link (file or URL) in clipboard
--clipboard         Put result link (file or URL) in clipboard
--no-open           Don't open result (with xdg-open) after success
--open              Open result with xdg-open after success
--no-save           Don't save result locally
--save              Save result locally
--no-upload         Don't upload
-u, --upload        Upload is a compatible uploader is set
--uploader <name>   Force this uploader (use -l to list them)
```

To customize *sss*, read the [dedicated help](./CUSTOMIZE.md).
