#!/usr/bin/env bash

do_open () {
  dbg "open: $OPENEXEC ${OPENARGS[@]} $1"
  $OPENEXEC "${OPENARGS[@]}" "$1" >/dev/null
}

# vim:set ts=2 sw=2 et:
