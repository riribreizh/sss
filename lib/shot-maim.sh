#!/usr/bin/env bash

do_shot () {
  local hc area file
  [[ $showcursor == 1 ]] || hc='--hidecursor'
  area="$1"
  file="$RUNDIR/$USER.$IMGFORMAT"
  dbg "shot: $SHOTEXEC ${SHOTARGS[@]} ${area:+--geometry $area} $hc $file"
  $SHOTEXEC "${SHOTARGS[@]}" ${area:+--geometry $area} $hc $file && echo $file
}

# vim:set ts=2 sw=2 et:
