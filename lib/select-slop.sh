#!/usr/bin/env bash

if [[ -z $SELECTARGS ]]; then
  SELECTARGS=(-q -b 4000 -r blur1,blur2,boxzoom)
  # ensure slop has the default shaders needed by sss
  [[ -d $XDG_CONFIG_HOME/slop ]] || mkdir -p "$XDG_CONFIG_HOME/slop"
  for s in blur1.vert blur1.frag blur2.vert blur2.frag boxzoom.vert boxzoom.frag; do
    [[ -r $XDG_CONFIG_HOME/slop/$s ]] || cp "$SSSDIR/shaders/$s" "$XDG_CONFIG_HOME/slop"
  done
fi

do_select () {
  local nd
  [[ $decorations == 1 ]] || nd='--nodecorations=2'
  dbg "select: $SELECTEXEC ${SELECTARGS[@]} $nd"
  $SELECTEXEC "${SELECTARGS[@]}" $nd
}

# vim:set ts=2 sw=2 et:
