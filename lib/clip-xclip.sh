#!/usr/bin/env bash

dbg "clipboard: $CLIPEXEC ${CLIPARGS[@]}"

do_clip () {
  if [[ $1 == 'set' ]]; then
    echo "$2" | $CLIPEXEC "${CLIPARGS[@]}" -selection clip
  else # assume get/paste
    ($CLIPEXEC "${CLIPARGS[@]}" -o -selection clip) > "$2"
  fi
}

# vim:set ts=2 sw=2 et:
