#!/usr/bin/env bash

dbg "MENUARGS: ${MENUARGS[@]}"
rofi_default_args="-i -dmenu -sep | -theme $SSSDIR/sss.rasi"

# do_menu
#   [-t {title}]
#   [-m {message}]
#   [-s {cursel index}]
#   [-h {highlight index}]
#   [-b] (add a '◀ Back' item)
#   (array of items)
do_menu () {
  local ret mesg sel hl back title menu
  while [[ $# -gt 0 ]]; do
    case $1 in
    -t) title="$2"; shift;;
    -m) mesg="$2"; shift;;
    -s) [[ "$2" ]] && sel="-sel $2"; shift;;
    -u) [[ "$2" ]] && hl="-u $2"; shift;;
    -b) back='|◀ Back';;
    *) break;;
    esac
    shift
  done
  [[ "$title" ]] || title="$SSSTITLE"
  [[ "$mesg" ]] && title="""$title
<span size=\"small\">$mesg</span>"""
  menu=$(IFS='|'; echo "$*")
  dbg "menu: $menu | $MENUEXEC $rofi_default_args -mesg \"$title\" ${MENUARGS[@]} $sel $hl"
  echo "$menu$back" | $MENUEXEC $rofi_default_args -mesg "$title" "${MENUARGS[@]}" $sel $hl
  case $? in
  0|10-28) ret=0;;
  *) ret=1;;
  esac
  return $ret
}

# vim:set ts=2 sw=2 et:
