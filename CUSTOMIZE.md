# Customize sss

Generate a user configuration either from the options menu or `sss -d`. The file is a *bash* script defining variables that will override **sss** defaults (and which will be overridden by command line options).

## Variables

#### INTERACTIVE
If set to `1`, tries to show a menu when possible. If set to `0`, **sss** will use its configuration to determine what to do. For example, if you call `sss -s`, you ask for a screenshot, so the main menu will be skipped, but with *INTERACTIVE* set to `1`, what to do with the screenshot (save, upload, open) will be asked through a menu.

#### DECORATIONS
When capturing a window, this tells **sss** if the window manager decorations should be included (`1`) or not (`0`).

#### SHOWCURSOR
Should the mouse cursor be included? Set to `1` to show where you point at :)

#### SAVEONDISK
Although the main usage is to save captures on disk, there should be situations when you just want to upload online. Setting this to `0` prevents **sss** to save a local copy.

#### CLIPBOARD
Tells **sss** to use **xclip** for copying the resulting URL to clipboard (remote URL if uploading, local file if not).

#### OPENONSUCCESS
Uses **xdg-open** at the end to open the result (the remote one if an upload was done).

#### UPLOAD
Do the remote upload by default (`1`). If no uploader is configured for the type of capture, this is automatically set to `0`.

#### IMGFORMAT
This must be image extensions supported by **maim** (usual formats are `png`, `jpg`).

#### IMGPREFIX
**sss** uses a timestamp to name saved images. This is what to put in front of this timestamp.

#### IMGPATH
The directory where **sss** will copy a local save.

#### IMGUPLOAD
The name of the plugin to use for uploading images.

#### VIDFORMAT
TODO

#### VIDPREFIX
**sss** uses a timestamp to name saved videos. This what to put in front of this timestamp.

#### VIDPATH
The directory where **sss** will copy a local save.

#### VIDUPLOAD
The name of the plugin to use for uploading videos.

#### PSTFORMAT
The text file extension to use on pastes.

#### PSTPREFIX
**sss** uses a timestamp to name saved pastes. This what to put in front of this timestamp.

#### PSTPATH
The directory where **sss** will copy a local save.

#### PSTUPLOAD
The name of the plugin to use for uploading pastes.

#### ROFIEXEC
The program to use as **rofi**. You can add command line switches there if needed.

#### ROFICUST
This will be used as the argument to *rofi*'s `theme-str` option, so you can control the menu look. See below for more information.

#### SLOPOPTS
Options to pass to the **slop** command (mostly for it's appearence). **sss** provides a default set of shaders to use (and copy them to the **slop** configuration directory), but you can use others, to totally disable them.

#### MAIMEXEC
The program to use as **maim**. You can add command line switches there if needed.

#### FFPMEGEXEC
The program to use as **ffmpeg**. You can add command line switches there if needed.

#### XCLIPEXEC
The program to use as **xclip**. You can add command line switches there if needed.

## SSS Rofi theme

**sss** has its own **rofi** theme file (*installation prefix*`/share/sss/sss.rasi`). It has a very simple scheme and you can easily change colors by just defining some variables through the **ROFICUST** configuration variable. You can of course override anything, it's up to you.

```css
* {
	spacing: 0;

	maincolor: #444444;
	textcolor: #dddddd;
	urgentcolor: #dd3333;

	transparent: #000000dd;
}
```

That's what is defined in `sss.rasi`, and you can easily change the flat grey color by something matching your desktop theme, by overriding the `maincolor`. For example to make it orange, set in your **sss** configuration file:
```sh
ROFICUST="* { maincolor: #bf616a; }"
```

![custom main color screenshot](./custom-color.png "sss with a custom main color")

The four variable are explicit enough to figure out what they relate ;-)
If you want to customize the look a bit more, look in `sss.rasi`.
